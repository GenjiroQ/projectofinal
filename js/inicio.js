    // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
  import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyBxsY945uwe0GrKzxHxdaywW3gwojkAzsA",
    authDomain: "sitioweb-4904f.firebaseapp.com",
    databaseURL: "https://sitioweb-4904f-default-rtdb.firebaseio.com",
    projectId: "sitioweb-4904f",
    storageBucket: "sitioweb-4904f.appspot.com",
    messagingSenderId: "938438915462",
    appId: "1:938438915462:web:d1adc814907b89b70d2807"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();
  const auth = getAuth();


  var btnLoguear = document.getElementById('btnLoguear');

  var email = "";
  var password = "";

  function leer(){
    email = document.getElementById('correo').value;
    password = document.getElementById('pass').value;
  }
  
  // Permite ver si el usuario esta conectado o no
  function comprobarAUTH(){
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // alert("Usuario detectado");
        document.getElementById('todo').style.display="block";
        // ...
      } else {
        // User is signed out
        // ...
        // alert("No se ha detectado un usuario");
        window.location.href="inicio.html";
      }
    });
  }
  
  // Si existen = entonces hacer acción
  
  if(window.location.href == "http://127.0.0.1:5500/html/admin.html"){
    window.onload = comprobarAUTH();
  }

  btnLoguear.addEventListener('click', (e)=>{
    leer();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;
        alert("¡INICIO DE SESIÓN EXITOSO!")
        window.location.href="admin.html";
        // ...
      })
      .catch((error) => {
        alert("DATOS INCORRECTOS")
        const errorCode = error.code;
        const errorMessage = error.message;
      });
  });