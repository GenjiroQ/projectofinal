// Configuración firebase

// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyBxsY945uwe0GrKzxHxdaywW3gwojkAzsA",
    authDomain: "sitioweb-4904f.firebaseapp.com",
    databaseURL: "https://sitioweb-4904f-default-rtdb.firebaseio.com",
    projectId: "sitioweb-4904f",
    storageBucket: "sitioweb-4904f.appspot.com",
    messagingSenderId: "938438915462",
    appId: "1:938438915462:web:d1adc814907b89b70d2807"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();


// Declaración de objetos
var btnInsertar = document.getElementById('btnInsertar');
var btnBuscar = document.getElementById('btnBuscarporID');
var btnActualizar = document.getElementById('btnActualizar');
var btnTodos = document.getElementById('btnTodos');
var btnLimpiar = document.getElementById('btnLimpiar');
var lista = document.getElementById('lista');
var listaP = document.getElementById('listaProductos');

var btnMostrarImagen = document.getElementById('verImagen');
var archivo = document.getElementById('archivo');


var ID = "";
var nombre = "";
var precio = "";
var descripcion = "";
var status = "";
var nombreIMG = "";
var url = "";

if(window.location.href == "http://127.0.0.1:5500/html/productos.html"){
  window.onload = todos();
}


function todos(){

    const db = getDatabase();
    const dbRef = ref(db, 'productos');

    onValue(dbRef, (snapshot) => {
        if(lista){
            lista.innerHTML = "";
        }
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            if(lista){
                
                lista.innerHTML = lista.innerHTML + "<div class='prod'> <img src="+ childData.url +" alt='si'><div><h3>" + childData.nombre + " (est: "+ childData.status + ")</h3><p>" + childData.descripcion +"</p><br><p>"+ childData.precio + "</p><p>" + childKey + "</p><button>COMPRAR</button></div></div>";
            }else if(listaP){
                if(childData.status == 0){
                    listaP.innerHTML = listaP.innerHTML + "<div class='prod'> <img src="+ childData.url +" alt='si'><div><h3>" + childData.nombre + "</h3><p>" + childData.descripcion +"</p><br><p>"+ childData.precio + "</p> <button>COMPRAR</button></div></div>";

                }else{

                }
            }


            // lista.innerHTML = lista.innerHTML + "<div class='prod'> <img src="+ childData.url +" alt='si'><div><h3>" + childData.nombre + " (est: "+ childData.status + ")</h3><p>" + childData.descripcion +"</p><br><p>"+ childData.precio + "</p><p>" + childKey + "<button>COMPRAR</button></div></div>";


            // console.log(childKey + ":");
            // console.log(childData.nombre);
        });
    },{
        onlyOnce: true
    });

}


function leerInputs(){
    ID = document.getElementById('ID').value;
    nombre = document.getElementById('nombre').value;
    precio = document.getElementById('precio').value;
    descripcion = document.getElementById('descripcion').value;
    status = document.getElementById('Status').value;
    url = document.getElementById('url').value;


    // alert("Matricula: " + matricula + " Nombre: " + nombre + " Carrera: " + carrera + " Genero: " + genero);
}


function insertarDatos(){

    leerInputs();

    set(ref(db,'productos/' + ID), {
        ID:ID,
        nombre:nombre,
        precio:precio,
        descripcion:descripcion,
        status:status,
        url:url


    }).then((response)=>{
        alert("Se agrego un registro con éxito");
    }).catch((error)=>{
        alert("Surgio un error: " + error);
    });
    

}

function mostrarDatos(){
    leerInputs();
    const dbref = ref(db);
    
    get(child(dbref,'productos/' + ID)).then((snapshot)=>{
        if (snapshot.exists()) {
            ID = snapshot.val().ID;
            nombre = snapshot.val().nombre;
            precio = snapshot.val().precio;
            descripcion = snapshot.val().descripcion;
            url = snapshot.val().url;
            status = snapshot.val().status;

            escribirInputs();
        }else{
            alert("No existe el ID");
        }
    }).catch((error)=>{
        alert("Surgió un error: " + error);
    });
}

function escribirInputs(){
    document.getElementById('ID').value = ID;
    document.getElementById('nombre').value = nombre;
    document.getElementById('precio').value = precio;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('Status').value = status;
    document.getElementById('url').value = url;

}

function actualizar(){

    leerInputs();

    update(ref(db,'productos/'+ ID),{
        ID:ID,
        nombre:nombre,
        precio:precio,
        descripcion:descripcion,
        status:status,
        url:url

    }).then(()=>{
        alert("Se realizó la actualización");
        mostrarDatos();
    }).catch(()=>{
        alert("Surgió un error: " + error);
    });
    

}

function limpiar(){

    lista.innerHTML="";
    ID = "";
    nombre = "";
    precio = "";
    descripcion = "";
    status = "";

    escribirInputs();

}

function cargarImagen(){

    // archivo seleccionado

    const file = event.target.files[0];
    const name = event.target.files[0].name;

    const storage = getStorage();
    const storageRef = refS(storage, 'Imagenes/' + name);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, file).then((snapshot) => {
        document.getElementById('imgNombre').value = name;
        alert("Se cargo el archivo");
      });
}

function descargarImagen(){

    archivo = document.getElementById('imgNombre').value;

    // import { getStorage, ref, getDownloadURL } from "firebase/storage";

    // Create a reference to the file we want to download
    const storage = getStorage();
    const storageRef = refS(storage, 'Imagenes/' + archivo);

    // Get the download URL
    getDownloadURL(storageRef)
    .then((url) => {
        document.getElementById('url').value = url;
        document.getElementById('imagen').src = url;
    })
    .catch((error) => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
        case 'storage/object-not-found':
            console.log("No existe el archivo")
            break;
        case 'storage/unauthorized':
            console.log("No tiene permisos");
            break;
        case 'storage/canceled':
            console.log("Se canceló o no tiene internet");
            break;

        // ...

        case 'storage/unknown':
            console.log("Error desconocido");
            break;
        }
    });

}




// Evento click
if(btnInsertar){
    btnInsertar.addEventListener('click', insertarDatos);
}

if(btnBuscar){
    btnBuscar.addEventListener('click', mostrarDatos);
}

if(btnActualizar){
    btnActualizar.addEventListener('click', actualizar);
}
if(btnTodos){
    btnTodos.addEventListener('click', todos);
}
if(btnLimpiar){
    btnLimpiar.addEventListener('click', limpiar)
}
if(btnMostrarImagen){
    btnMostrarImagen.addEventListener('click', descargarImagen)
}



if(archivo){
    archivo.addEventListener('change', cargarImagen);
}
